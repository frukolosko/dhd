-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 24, 2020 at 01:20 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `opti`
--

-- --------------------------------------------------------

--
-- Table structure for table `ayar`
--

CREATE TABLE `ayar` (
  `id` int(11) NOT NULL,
  `titile` varchar(50) NOT NULL,
  `logo` text NOT NULL,
  `keywords` varchar(250) NOT NULL,
  `aciklama` varchar(210) NOT NULL,
  `bakim` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ders`
--

CREATE TABLE `ders` (
  `id` int(11) NOT NULL,
  `video_url` varchar(500) NOT NULL,
  `vide_aciklama` varchar(500) NOT NULL,
  `video_baslik` varchar(50) NOT NULL,
  `test_id` int(11) NOT NULL,
  `konu_id` int(11) NOT NULL,
  `yap` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ders`
--

INSERT INTO `ders` (`id`, `video_url`, `vide_aciklama`, `video_baslik`, `test_id`, `konu_id`, `yap`) VALUES
(1, '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/4ikFklV5i8U\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', 'Bu derste sayma işlemlerini göreceksiniz', 'Sayma', 2, 1, 0),
(2, '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/6759W3Lb3_o\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', 'Bu derste sayma işlemlerini göreceksiniz', 'Sıralı sayma', 2, 1, 0),
(3, '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/flEkSAGNaUQ\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', 'Bu derste sayma işlemlerini göreceksiniz', 'Şekillerle sayma', 2, 1, 0),
(4, '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/2EeNJz26qnI\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', 'Bu derste sayma işlemlerini göreceksiniz', 'Nesneleri Sayma Alıştırması', 2, 1, 0),
(5, '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/dNxQJ5zsyEM\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', 'Alfabe öğrenelim', 'eğlenceli Alfabe ', 1, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ders_konu`
--

CREATE TABLE `ders_konu` (
  `id` int(11) NOT NULL,
  `baslik` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ders_konu`
--

INSERT INTO `ders_konu` (`id`, `baslik`) VALUES
(1, 'matematik'),
(2, 'türkçe'),
(3, 'Fen bilimleri');

-- --------------------------------------------------------

--
-- Table structure for table `gelisim`
--

CREATE TABLE `gelisim` (
  `id` int(11) NOT NULL,
  `gorev` varchar(50) NOT NULL,
  `acıklama` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kitap`
--

CREATE TABLE `kitap` (
  `id` int(11) NOT NULL,
  `kitap_url` varchar(250) NOT NULL,
  `baslik` varchar(50) NOT NULL,
  `aciklama` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `mot_mes`
--

CREATE TABLE `mot_mes` (
  `id` int(11) NOT NULL,
  `merin` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sik`
--

CREATE TABLE `sik` (
  `id` int(11) NOT NULL,
  `soru_id` int(11) NOT NULL,
  `sira` int(10) NOT NULL,
  `puan` varchar(250) NOT NULL,
  `metni` varchar(250) NOT NULL,
  `sik_turu` int(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `soru`
--

CREATE TABLE `soru` (
  `id` int(11) NOT NULL,
  `metni` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `takvim`
--

CREATE TABLE `takvim` (
  `id` int(11) NOT NULL,
  `uye_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `saat` date NOT NULL,
  `baslik` varchar(50) NOT NULL,
  `aciklama` varchar(100) NOT NULL,
  `bitis_tarih` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `id` int(11) NOT NULL,
  `aciklama` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `tc` int(11) NOT NULL,
  `ad` varchar(50) NOT NULL,
  `soyad` varchar(50) NOT NULL,
  `mail` varchar(250) NOT NULL,
  `telefon` varchar(12) NOT NULL,
  `d_tarih` date NOT NULL,
  `il` varchar(32) NOT NULL,
  `parola` varchar(64) NOT NULL,
  `son_giris` date NOT NULL,
  `danisman` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`tc`, `ad`, `soyad`, `mail`, `telefon`, `d_tarih`, `il`, `parola`, `son_giris`, `danisman`) VALUES
(123456, 'mevlüt furkan', 'demir', 'mfd@mfd.com', '123465', '2020-04-04', 'konya', '123456', '2020-04-01', 1);

-- --------------------------------------------------------

--
-- Table structure for table `yap_gorev`
--

CREATE TABLE `yap_gorev` (
  `id` int(11) NOT NULL,
  `gorev_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ders`
--
ALTER TABLE `ders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ders_konu`
--
ALTER TABLE `ders_konu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gelisim`
--
ALTER TABLE `gelisim`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kitap`
--
ALTER TABLE `kitap`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mot_mes`
--
ALTER TABLE `mot_mes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sik`
--
ALTER TABLE `sik`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `soru`
--
ALTER TABLE `soru`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `takvim`
--
ALTER TABLE `takvim`
  ADD PRIMARY KEY (`id`,`baslik`);

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`tc`);

--
-- Indexes for table `yap_gorev`
--
ALTER TABLE `yap_gorev`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ders`
--
ALTER TABLE `ders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ders_konu`
--
ALTER TABLE `ders_konu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `gelisim`
--
ALTER TABLE `gelisim`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kitap`
--
ALTER TABLE `kitap`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mot_mes`
--
ALTER TABLE `mot_mes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sik`
--
ALTER TABLE `sik`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `soru`
--
ALTER TABLE `soru`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `takvim`
--
ALTER TABLE `takvim`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `test`
--
ALTER TABLE `test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `yap_gorev`
--
ALTER TABLE `yap_gorev`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
