<?php 

include 'header.php';


 ?>

    <!-- service_area_start  -->
    <div class="service_area">
        <div class="container">
            <div class="row justify-content-center ">
                <div class="col-lg-7 col-md-10">
                    <div class="section_title text-center mb-95">
                        <h3>Neden Disleksi Okulu</h3>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-4 col-md-6">
                    <div class="single_service">
                         <div class="service_thumb service_icon_bg_1 d-flex align-items-center justify-content-center">
                             <div class="service_icon">
                                 <img src="img/service/service_icon_1.png" alt="">
                             </div>
                         </div>
                         <div class="service_content text-center">
                            <h3>Kişileştirilmiş Öğrenim</h3>
                            <p>Öğrenciler kendi hızlarında alıştırma yapabilir, konulardaki eksikliklerini giderebilir ve öğrenme süreçlerini hızlandırabilirler.</p>
                         </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single_service active">
                         <div class="service_thumb service_icon_bg_1 d-flex align-items-center justify-content-center">
                             <div class="service_icon">
                                 <img src="img/service/service_icon_2.png" alt="">
                             </div>
                         </div>
                         <div class="service_content text-center">
                            <h3>Güvenilir İçerik</h3>
                            <p>Uzmanlar tarafından oluşturulan Disleksi Academy kütüphanesi, matematik, fen bilimleri ve daha fazlasını içeriyor. İçeriğimizin tamamı her zaman ücretsizdir.
                            </p>
                         </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single_service">
                         <div class="service_thumb service_icon_bg_1 d-flex align-items-center justify-content-center">
                             <div class="service_icon">
                                 <img src="img/service/service_icon_3.png" alt="">
                             </div>
                         </div>
                         <div class="service_content text-center">
                            <h3>Öğrenmeye Yardımcı Araçlar</h3>
                            <p>Öğretmenler Disleksi Academy'nin yardımı ile öğrencilerinin bilgi eksikliklerini tespit edebilir, kişiye özel ödev verebilir ve böylece her öğrencinin ihtiyacını tek tek karşılayabilirler.</p>
                         </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- service_area_end -->

    <!-- pet_care_area_start  -->
    <div class="pet_care_area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 col-md-6">
                    <div class="pet_thumb">
                        <img src="img/about/deneme.jpg" alt="">
                    </div>
                </div>
                <div class="col-lg-6 offset-lg-1 col-md-6">
                    <div class="pet_info">
                        <div class="section_title">
                            <h3><span>Her şeyi öğrenebilirsin. </span> <br>
                                Haydi Başla</h3>
                            <p>Gelişim odaklı bir yaklaşım ile matematik, fen bilimleri ve diğer konularda kendinizi sürekli geliştirin..</p>
                            <a href="about.html" class="boxed-btn3">Öğrenmek isteyenler buraya</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- pet_care_area_end  -->

    <!-- adapt_area_start  -->

    <!-- adapt_area_end  -->

    <!-- testmonial_area_start  -->
    <div class="testmonial_area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="textmonial_active owl-carousel">
                        <div class="testmonial_wrap">
                            <div class="single_testmonial d-flex align-items-center">
                                <div class="test_thumb">
                                    <img src="img/testmonial/1.png" alt="">
                                </div>

                                <div class="test_content">
                                    <h4>Nevin Altıntürk</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exerci.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- testmonial_area_end  -->

    <!-- team_area_start  -->
<!--    <div class="team_area">-->
<!--        <div class="container">-->
<!--            <div class="row justify-content-center ">-->
<!--                <div class="col-lg-6 col-md-10">-->
<!--                    <div class="section_title text-center mb-95">-->
<!--                        <h3>DESTEKÇİLERİMİZ</h3>-->
<!--                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="row justify-content-center">-->
<!--                <div class="col-lg-4 col-md-6">-->
<!--                    <div class="single_team">-->
<!--                        <div class="thumb">-->
<!--                            <img src="img/team/1.png" alt="">-->
<!--                        </div>-->
<!--                        <div class="member_name text-center">-->
<!--                            <div class="mamber_inner">-->
<!--                                <h4>Rala Emaia</h4>-->
<!--                                <p>Senior Director</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-lg-4 col-md-6">-->
<!--                    <div class="single_team">-->
<!--                        <div class="thumb">-->
<!--                            <img src="img/team/2.png" alt="">-->
<!--                        </div>-->
<!--                        <div class="member_name text-center">-->
<!--                            <div class="mamber_inner">-->
<!--                                <h4>jhon Smith</h4>-->
<!--                                <p>Senior Director</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-lg-4 col-md-6">-->
<!--                    <div class="single_team">-->
<!--                        <div class="thumb">-->
<!--                            <img src="img/team/3.png" alt="">-->
<!--                        </div>-->
<!--                        <div class="member_name text-center">-->
<!--                            <div class="mamber_inner">-->
<!--                                <h4>Rala Emaia</h4>-->
<!--                                <p>Senior Director</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
    <!-- team_area_start  -->

    <div class="contact_anipat anipat_bg_1">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="contact_text text-center">
                        <div class="section_title text-center">
                            <h3>Hemen Bize Katılın</h3>
                            <p>Because we know that even the best technology is only as good as the people behind it. 24/7 tech support.</p>
                        </div>
                        <div class="contact_btn d-flex align-items-center justify-content-center">
                            <style>
                                a{
                                    margin-left: 10px;
                                }
                            </style>
                            <a href="#" class="boxed-btn4">Öğrenciler</a>
                            <a href="#" class="boxed-btn4">Veliler</a>
                            <a href="#" class="boxed-btn4">Öğretmenler</a>
                            <a href="#" class="boxed-btn4">Destek</a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php 

include 'footer.php';

 ?>