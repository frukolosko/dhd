

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Blank Page</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Blank Page</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Title</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">

                <?php

                $query = $db->query("SELECT * FROM ders_konu ", PDO::FETCH_ASSOC);
                if ( $query->rowCount() ){
                foreach( $query as $row ){?>



                <div class="col-12">
                    <!-- jQuery Knob -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <i class="far fa-chart-bar"></i>
                                <?php echo  $row["baslik"] ?>
                            </h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-6 col-md-3 text-center">
                                    <div class="knob-label">Genel İlerleme</div>
                                    <div style="display:inline;width:90px;height:90px;"><canvas width="90" height="90"></canvas><input type="text" class="knob" value="30" data-width="90" data-height="90" data-fgcolor="#3c8dbc" style="width: 49px; height: 30px; position: absolute; vertical-align: middle; margin-top: 30px; margin-left: -69px; border: 0px none; background: rgba(0, 0, 0, 0) none repeat scroll 0% 0%; font: bold 18px Arial; text-align: center; color: rgb(60, 141, 188); padding: 0px; -moz-appearance: none;"></div>

                                </div>

                                <p> Konular:</p>
                                <!-- max 5 row -->

                                <ul>


                             <?php
                             $konu=$row["id"];
                             $query = $db->query("SELECT * FROM ders where konu_id=$konu ", PDO::FETCH_ASSOC);
                                if ( $query->rowCount() ){
                                foreach( $query as $row ){?>

                                    <li><a href="" data-toggle="modal" data-target="#modal<?php echo  $row["id"] ?>">   <?php echo  $row["video_baslik"] ?> </a>  </li>
                                    <div class="modal fade" id="modal<?php echo  $row["id"] ?>" style="display: none;" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title"><?php echo  $row["video_baslik"] ?> </h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                   <div class="card-title">  <?php echo  $row["vide_aciklama"] ?> </div>
                                                    <div ><hr></div>
                                                  <div class="modal-body table-responsive-xl ">
                                                      <?php echo  $row["video_url"] ?>   </div>

                                                </div>
                                                <div class="modal-footer justify-content-between">
                                                    <a href=" <?php echo  $row["test_id"] ?>">
                                                    <button type="button" class="btn btn-primary">Tamam</button>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                <?php }}?>


                                </ul>
                                <div class="btn-group-toggle " style="padding: 35px">
                                    <a href="#">
                                    <button type="button"  class="btn btn-block bg-gradient-info btn-lg fa fa-play-circle " >Devam edelim</button>
                                    </a>
                                </div>

                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>


<?php }}?>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                Footer
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrap


